# How to make a Blockchain Nation?

This year, the main blockchain projects will deploy their infrastructures across the globe and choose a position in Europe
Beyond technology, blockchains bring a political solution for alignment of interests between nations (wishing to live together) and governments (authority guaranteeing central services on a territory).

Government must avoid his obsolescence to the nations’ eyes, facing future “Estonia”. It’s what first uses of blockchains are. And, strategically, Government has to attract players who will support blockchain infrastructures on which tomorrow’s ledgers, exchanges and organizations will be based.

In the actual global and social context, both objectives are linked. With the usage of blockchains, populations would share the same transaction tool as markets, trusted way. There is a major interest in sharing decentralized process between countries, in order to get out of the incapacity of agreeing together, respecting sovereign interests. Finally, treatment inequalities, distrust towards institutions and markets should find the beginning of an answer with a social shared tool.

## How to create the fundamentals of an accessibility to blockchain technologies?

As far as technological innovation goes, we often introduced blockchains equivalent of the 95’s Internet. Blockchains bring opening of public and distributed ledgers on the networks thanks to a cryptographic arsenal. The “Level 1” is operational and the next abstraction levels should follow until a real usability.

But there is no equivalence between people and markets’ access conditions and those from today or tomorrow. There won’t be a “next level” without defining a method to spread a disruptive technology which supports a durable and social impact. Adoption involves iterations between research, developers, consulting and industrial, institutional and societal standards.

Unlike GAFAMs’ services, blockchains are slow, costs are systemic, and there is no guarantor, except the code. Government has to help education and learning along, and promote a decentralized process, a neutral ground, which protects private life; because spreading a vision is already reinforcing a way of doing.

## The essential cooperation between pioneers and states

Blockchains rest on 40 years of academic work in mathematics, cryptography, computer science and telecommunications. This leads to new fields of technological, economic and organizations research. Usually, a group of researchers describes an innovative architecture of “blockchain” type, and then an open source project allows developers to make an operational version. Currently, these projects need to cooperate with states to popularize these work.

For example, developers can learn how to protect themselves from global espionage and forced culture. It is about key notions to end 10 years of failure of cybersecurity and information isolation. It is not only technical and the Government must help the intersecting spreading of this type of content. 
Schools, influenced by SSII, must supply several courses to match with the numeric needs, and will limit themselves to one or two blockchains. The focus must be in advance, on fundamental principles. Researchers on whom(whose?) these next “mega” autonomous infrastructures rest on renew every 7 years. Some projects already hired the few concerned PhDs. We have to encourage dissertations/thesis about decentralization of the organisations, management delegated to algorithms, contributive & community economies, consensus, cryptography and privacy, cybersecurity, mesh networks, token diplomacy.. and catch up for being behind about these training. 
It is necessary to create the conditions of education, teaching to corporates and institutions, as well as making learning easier. It is about commiting the country about its opportunities’ definition. Key features of freedom of learning management are not defined, especially their translation for the company. However, assimilation of this technology can contribute to it.

## Make the practical, practical

For an infrastructure, the simple fact of working isn’t enough, there is a whole ecosystem of tools, documentation and demonstrators to create an “usage”. This requires to make the code easier to understand and use, and more widespread. We need to publish some articles that aims to make the technical part easier to get, follow talents who are interested by the subject, and commit them on events, hackathons, and in online communities.

These service opportunities create a demand in training. We need to train tutors because the attraction of a government rest on its ability to create capacity. To increase the number of informed people, the Government should refund tutors’ training. In the not so distant future, Government should support consulting, service and distribution networks to specialize on usages and concerned industries. What we are playing for is to organize a convergence with markets’ makers (like Gartner for example), in order to identify a role in architectures. 

## Scaling up 

Some fields already have technologic leaders, for example Bitcoin in the banking sector, with a robust solution since 10 years. But ecosystems which will support an external economic development from original communities are struggling to emerge. Blockchain projects worked to make the development friendly without too much blockchain expertise, but there is nearly nothing allowing integration of decentralized networks into current architectures, usages and markets.

The opportunity is to the height of the challenge, because a company with a first adopter status gets an exclusive position in accessing innovations. It’s the entry point for major companies, talents and sponsoring programs. Their activity is a key for the deployment of blockchain projects which don’t have the ability to make these marketing process via their community around the world. It is important for the government to support services (job carriers) which spread the technology.

This phase must be politically accompanied in order to reassure investors determined to scale up. Government has to reserve a significant number of its calls for bids to implement blockchains and communicate about its initiatives. It is about working to an image of technology which can be identifiable in markets and medias to reach an influence on industrialists. Government must promote a standardisation with a focus on projects aiming to either make the value chain fairer or contributing to advance work, or offering innovations for smart cities.

## To conclude

The world is changing deeper than we can perceive. The work done on crypto assets regulations is necessary but maybe premature, it is useless to change the rules before the game even exist. We may as well focus the efforts on countries’ real and current needs. Tensions emerge between lived realities and new populations’ aspirations. Blockchain technologies of which fundamentals exist since several tens of years, are experiencing a social momentum. Tomorrow’s infrastructures are deciding today, depending on ecosystems’ ability, or not, to help this major transformation and recognize a greater independence of communities.


